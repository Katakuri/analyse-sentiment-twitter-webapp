import { Component, OnInit } from '@angular/core';
import {ApisearchService} from '../service/apisearch.service'

@Component({
  selector: 'app-liste-tweets',
  templateUrl: './liste-tweets.component.html',
  styleUrls: ['./liste-tweets.component.scss']
})
export class ListeTweetsComponent implements OnInit {

  listTweets = [];
  newList : any;
  seconde : Number;
  load : Boolean;

  constructor(
   public searchTweets : ApisearchService
  ) { }

  ngOnInit() {
    this.load = this.searchTweets.loading;
    this.searchTweets.currentData.subscribe((res : any) => {
      this.listTweets = res.text;
      this.searchTweets.loading = false;
    })
    }

}
