import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeTweetsComponent } from './liste-tweets.component';

describe('ListeTweetsComponent', () => {
  let component: ListeTweetsComponent;
  let fixture: ComponentFixture<ListeTweetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeTweetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeTweetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
