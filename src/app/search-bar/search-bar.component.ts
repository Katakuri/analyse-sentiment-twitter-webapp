import { Component, OnInit } from '@angular/core';
import {ApisearchService} from '../service/apisearch.service';
import { FormGroup,Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  
  tweetsRecovred = [];
  form: FormGroup;
  constructor(
    private   tweetService : ApisearchService

  ) { }

  ngOnInit() {
    // console.log(this.tweetService.resultTweets);
    this.form = new FormGroup({
      motRechercher : new FormControl('', [Validators.required, Validators.maxLength(25)]),
      langue : new FormControl('', [Validators.required, Validators.maxLength(15)])
    });
  }


  submit(form) {
    // console.log(form.value)
    this.tweetService.loading = true;
    this.tweetService.getTweets(form.value.motRechercher, form.value.langue).then((res)=>
    this.tweetService.changeData(this.tweetService.resultTweets)
    );

  }


}
