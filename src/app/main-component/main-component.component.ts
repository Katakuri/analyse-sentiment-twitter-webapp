import { Component, OnInit } from '@angular/core';
import {ApisearchService} from '../service/apisearch.service'

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.scss']
})
export class MainComponentComponent implements OnInit {

  constructor(private apiSearch : ApisearchService) { }

  ngOnInit() {
  }

  showStats(){
    return this.apiSearch.showStats;
  }

}
