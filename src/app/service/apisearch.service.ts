import { Injectable } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { HttpClient } from '@angular/common/http';
// import {Observable,of, from } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApisearchService {

  showStats = false;
  resultTweets = [];
  
  url = "http://localhost:5000/api/getTweets/"
  
  private dataSource = new BehaviorSubject(this.resultTweets);
  currentData = this.dataSource.asObservable();
  loading = false;

  counter = interval(1000)
  constructor(private http : HttpClient) { }


  changeData(data: any) {
    // console.log(data)
    this.dataSource.next(data);
    this.showStats = true;
  }


  getTweets(word : string, lang : string){
    const promise = new Promise((resolve, reject) => {
      this.http.get<any>(this.url+word+'/'+lang)
      .toPromise()
      .then(
        res => { console.log(res);
        this.resultTweets = res;
        resolve();
        },
        msg => {
          reject();
        }
      );
     });
     return promise;
  
  }

  getPositifsTweets(data){
    let positiveTweets = []
    data.forEach(element => {
      if(element.avis.score > 0){
        positiveTweets.push(element);
      }
    });
    return positiveTweets
  }

  getNegatifsTweets(data){
    let negatifTweets = []
    data.forEach(element => {
      if(element.avis.score < 0){
        negatifTweets.push(element);
      }
    });
    return negatifTweets
  }

  getNeutralTweets(data){
    let neutralTweets = []
    data.forEach(element => {
      if(element.avis.score === 0){
        neutralTweets.push(element);
      }
    });
    return neutralTweets
  }

}
