import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import {ApisearchService} from '../service/apisearch.service';
import { BehaviorSubject } from 'rxjs';



@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

  listTweets = [];
  private dataSource = new BehaviorSubject(this.listTweets);
  currentData = this.dataSource.asObservable();


  changeData(data: any) { 
    // console.log(data)
    this.dataSource.next(data);
  }

  public doughnutChartLabels: Label[] = ['Tweets Négatifs', 'Tweets positifs', 'Tweets neutres'];
  public doughnutChartData: MultiDataSet = [
    [10, 20, 30]
  ];
  public doughnutChartType: any = 'doughnut';



  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    yAxisID : 'OK'
  };
  public barChartLabels = ['Répartition des Tweets'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    {data: [100], label: 'Tweets négatifs'},
    {data: [28], label: 'Tweets positifs'},
    {data: [28], label: 'Tweets neutres'}
  ];

  constructor(   
    private searchTweets : ApisearchService
    ) {}

  ngOnInit() {
     this.searchTweets.currentData.subscribe((res : any) => {

      this.listTweets = res.text;
      this.changeData(res.text);
      
      this.currentData.subscribe((res : any) => {
        // console.log(res);
        if(res !== undefined){
          //barChart
          let negativeTweets = this.searchTweets.getNegatifsTweets(res).length;
          let positiveTweets = this.searchTweets.getPositifsTweets(res).length;
          let neutralTweets = this.searchTweets.getNeutralTweets(res).length;

          this.barChartData[0].data = [negativeTweets]
          this.barChartData[1].data = [positiveTweets]
          this.barChartData[2].data = [neutralTweets]

          //doughnutChart
          this.doughnutChartData = [[negativeTweets, positiveTweets, neutralTweets]]
        }
      })
    }, 
    (err) => console.log(err),
    () => {
      // console.log('OK')
    });
  }

}
