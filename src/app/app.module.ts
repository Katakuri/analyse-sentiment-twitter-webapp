import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SearchBarComponent} from './search-bar/search-bar.component';
import {MaterialModule} from './angular-material'
import {ApisearchService} from './service/apisearch.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListeTweetsComponent } from './liste-tweets/liste-tweets.component';
import { MainComponentComponent } from './main-component/main-component.component';
import { StatsComponent } from './stats/stats.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchBarComponent,
    ListeTweetsComponent,
    MainComponentComponent,
    StatsComponent
  ],
  imports: [
    ChartsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule, 
    ReactiveFormsModule,
    ChartsModule
  ],
  providers: [ApisearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
